## Taperfuga: portable BLDC Centrifuge for field work

Full and updated documentation at: https://docs.openlabautomata.xyz/en/Mini-Docs/labware/Centrifuge

> Barebones tupergüare-style centrifuge.

It's awesome:

![bldc_taperfuga.jpg](./images/bldc_taperfuga.jpg)

Conceptual overview:

![tupperfuga.svg](images/tupperfuga.svg)

### Features and Caveats

Features:

- Portable: uses a small LiPo battery for power, can be carried in a backpack (even while running), upwards, sideways, or upside-down.
- Cool:
    - Swing-bucket style rotor.
    - Uses a BLDC from an old HDD.
- Fast-ish: at least 3000 rpm, probably 6000 rpm or more.

Caveats:

- Not so low-cost: you can make a centrifuge with less complicated motors (e.g. DC motor instead of a BLDC motor) and controllers (i.e. simple MOSFET circuit, instead of an ESC module).

### Video

https://www.youtube.com/watch?v=Lnsp6m0MHxk

### Bill of materials

- 10 cm wire
- 3 Crocodile clip / connectors
- M3 screws and nuts.
- 11.1V Li-po battery (3S).
- ESC for BLDC motors.
- A suitable plastic container with lid (a.k.a. a "tupper").
- Servo tester.
- Plastic foam (e.g. polyethylene foam).
- BLDC motor, either from a discarded HDD or a drone/UAV motor.

### Tools

- Soldering iron.
- M3 tap and electric drill.
- Double-sided and electrical tape.
- Screwdrivers, M2.5 and M3 hex keys, pliers.
- Multimeter.
- Hot glue gun.
- 3D-printer.

### Assembly

#### Step 1: get a motor

1. Solder wires to each copper pad.
2. If you are reusing a BLDC motor from an old HDD with 4 wires, use a multimeter to find the wire corresponding to the "star point", this wire won't be used. Solder "crocodile clips" or other suitable connectors to the ends of the three phase wires.
3. If you are reusing a BLDC motor from an old HDD, thread it's mounting holes with an M3 tap, and attach long M3 screws to them.

> BLDCs from newer HDDs cannot be separated from the aluminium base, and require heavy-duty hardware surgery for removal.

- Hot to disassemble an HDD and extract the BLDC motor: https://hackaday.com/2016/02/03/hard-drive-disassembly-is-easy-and-rewarding/
- Select and purchase a new BLDC motor: https://learn.adafruit.com/adafruit-motor-selection-guide/brushless-dc-motors
- Ask a friend if they have a spare :)

#### Step 2: print the rotor and purchase a container

1. 3D-print the rotor. FreeCAD files available [here](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master/models/labware/bldc_centrifuge). Currently only fits a BLDC motor from an old HDD.
2. Insert short M3 screws into the slots, such that their threads protrude a bit into the slots. then place a "swing" for 1.5mL tubes, and place a tube in it. Adjust the screws until they block the tube's pivoting, and then release them a bit such that the tube can swing freely, but is also well supported.
3. Use the rotor assembly to find a suitable plastic container. It must be wide enough to fit the rotor and the tubes when "fully swinged out".

![bldc-rotor.png](images/bldc-rotor.png)

> This rotor holds up to 6 tubes.

 > **Warning**: take extra precautions to ensure that the adapters are properly placed, never use the centrifuge without its lid, and **do not** use the adapters for 0.2mL tubes (instead you can improvise an adapter by cutting off the lids from 1.5mL and 0.5mL tubes, and stacking them).

#### Step 3: Attach the motor and rotor

1. BLDC motors from old HDDs can have 3 mounting holes, which can be threaded with an M3 tap.
2. Attach the motor to the plastic container by making holes trhough the bottom of the container. 
    - The rotor must be well centered and at a suitable height, allowing the tubes to hang and swing without colliding with the walls of the container (or other components).
    - In the picture below you can see I used a pair of nuts on each screw, which are tightened against each other to prevent the motor from sliding downwards. Three more nuts were tightened on the other side of the container's base to attach the motor firmly to it.
3. Add plastic foam pads to the base of the container, using a hot glue gun.
4. Attach the rotor using two M2.5 screws.

![img_20230906_114608.jpg](./images/img_20230906_114608.jpg)

> Note: the polystyrene foam immediately below the motor can be omitted for this build, it was originally meant to dampen vibrations.

![img_20230906_114345.jpg](images/img_20230906_114345.jpg)

> Note: the polystyrene foam below the tupper will help dampen vibrations.

![img_20230906_114243.jpg](images/img_20230906_114243.jpg)

> The rotor is atacched to the motor with a pair of M2.5 screws. Other HDD BLDCs might be threaded differently.

#### Step 4: connect the electronics

1. Connect the BLDC's three phases to the ESC controller. You can isolate the ends of the crocodile clips from each other by sliding in some heat-shrink around them (without applying heat) or using some electrical tape.
2. Attach the ESC to the container's bottom, tucking everything with tape or hot glue, to prevent the tubes from hitting them when spinning and swinging.
3. Make a hole through the side of the container to pass the ESC's power wires and control cable.
4. Connect the control cable to the servo tester, and glue the servo tester to the outside of the container.
5. Connect the battery to the ESC's power connector (there are different connectors, and you may need to swap them) and tape the battery to the outside of the container. Otherwise, solder a plug connector to the ESC and use a regular 12V power supply, capable of delivering at least 2A.

![img_20230906_114445.jpg](./images/img_20230906_114445.jpg)

![img_20230906_114337.jpg](./images/img_20230906_114337.jpg)

![img_20230906_114414.jpg](./images/img_20230906_114414.jpg)

![img_20230906_114349.jpg](./images/img_20230906_114349.jpg)
